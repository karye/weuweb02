# Rita med sprites

![sprite.png](<../.gitbook/assets/image (1).png>)

{% tabs %}
{% tab title="sprite.html" %}
```javascript
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Tilesheet</title>
    <link rel="shortcut icon" href="#">
    <link rel="stylesheet" href="./sprite.css">
</head>
<body>
    <canvas></canvas>
    <script>
        // Element vi arbetar med
        const eCanvas = document.querySelector("canvas");

        // Ställ in bredd och storlek
        eCanvas.width = 800;
        eCanvas.height = 600;

        // Starta canvas rityta
        var ctx = eCanvas.getContext("2d");

        var tileSheet = new Image();
        tileSheet.src = "./sprite.png";

        tileSheet.addEventListener("load", function () {

            // Rita 1:a rutan: 0, 0
            ctx.drawImage(tileSheet, 0, 0, 32, 32, 50, 50, 50, 50);

            // Rita 2:a rutan: 32, 0
            ctx.drawImage(tileSheet, 32, 0, 32, 32, 50, 100, 50, 50);

            // Rita 4:e rutan: 32, 0
            ctx.drawImage(tileSheet, 96, 0, 32, 32, 50, 150, 50, 50);

            // Rita ut en väg
            for (var i = 0; i < 10; i++) {
                var x = i * 50;
                ctx.drawImage(tileSheet, 0, 0, 32, 32, x, 0, 50, 50);
            }

            // Rita ut en vägg
            for (var i = 0; i < 10; i++) {
                var x = i * 50;
                ctx.drawImage(tileSheet, 32, 0, 32, 32, x, 200, 50, 50);
            }

            var karta = [0, 1, 3, 1, 0];
            for (var i = 0; i < karta.length; i++) {
                var x = i * 50;
                var ruta = karta[i] * 32;
                ctx.drawImage(tileSheet, ruta, 0, 32, 32, x, 300, 50, 50);
            }
        });
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="sprite.css" %}
```css
body {
    background: #000;
}
canvas {
    background: #d1d1d1;
    margin: 10px auto;
    display: block;
    border: 1px solid coral;
}
```
{% endtab %}
{% endtabs %}
