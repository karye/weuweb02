# Förbättrad animation

```javascript
// Element vi arbetar med
const eCanvas = document.querySelector("canvas");

// Ställ in bredd och storlek
eCanvas.width = 800;
eCanvas.height = 600;

// Starta canvas rityta
var ctx = eCanvas.getContext("2d");

var tileSheet = new Image();
tileSheet.src = "./tank-sheet.png";

var tankRutor = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8];
var i = 0;

function ritaTank() {
    var x = Math.floor(tankRutor[i] % 8) * 32;
    var y = Math.floor(tankRutor[i] / 8) * 32;
    ctx.drawImage(tileSheet, x, y, 32, 32, 250, 50, 50, 50);

    i++;
    if (i == tankRutor.length) {
        i = 0;
    }
}

// Animationsloopen
function gameLoop() {
    // Rensa canvas
    ctx.clearRect(0, 0, eCanvas.width, eCanvas.height);

    ritaTank();

    requestAnimationFrame(gameLoop);
}

// Starta spelet
gameLoop();

// https://www.oreilly.com/library/view/html5-canvas/9781449308032/ch04.html
```
