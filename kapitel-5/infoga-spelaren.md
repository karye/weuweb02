# Infoga spelaren

## Spelaren blir Pikachu

* Spelaren kan styra Pikachu till vänster och höger med pil-tangenter

```javascript
/* ****************************** */
/*           Objekten             */
/* ****************************** */

var pikachu = {
    x: Math.random() * 1000,
    y: 900,
    key: "",
    bild: new Image()
};
```

## Pikachus position

```javascript
/* ****************************** */
/*            Funktioner          */
/* ****************************** */

function ritaPikachu() {
    switch (pikachu.key) {
        case "ArrowRight":
            pikachu.x += 5;
            break;
        case "ArrowLeft":
            pikachu.x -= 5;
            break;
    }
    ctx.drawImage(pikachu.bild, pikachu.x, pikachu.y, 75, 75);
}
```

## Lyssna på piltangenter

```javascript
// Lyssna på input
window.addEventListener("keydown", function (e) {
    pikachu.key = e.key;
});
window.addEventListener("keyup", function (e) {
    pikachu.key = "";
});
```
