# Spelet Invader

## Grafik

* Ladda ned ikoner från [https://icons8.com/icon/set/game/color](https://icons8.com/icon/set/game/color)

## Grundkoden

{% tabs %}
{% tab title="pikachu.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Canvas</title>
    <link rel="stylesheet" href="./pikachu.css">
</head>
<body>
    <div class="kol2">
        <canvas></canvas>
        <div>
            <h1>Rädda Pikachu</h1>
        </div>
    </div>
    <script src="pikachu1.js"</script>
</body>
</html>
```
{% endtab %}

{% tab title="pikachu.css" %}
```css
body {
    background: slategrey;
    font-family: sans-serif;
}
canvas {
    width: 800px;
    height: 600px;
    border: 1px solid salmon;
    display: block;
    margin: 10px auto;
    background: #FFF;
}
.kol2 {
    display: grid;
    grid-template-columns: auto auto;
    gap: 10px;
    text-align: center;
    padding: 20px;
}
.kol3 {
    display: grid;
    grid-template-columns: auto auto auto;
    gap: 10px;
}
button {
    font-size: 40px;
    cursor: pointer;
}
```
{% endtab %}
{% endtabs %}

### Animationsloopen

```javascript
// Ikoner från https://icons8.com/icon/set/game/color
// Ställ in canvas
const eCanvas = document.querySelector("canvas");
eCanvas.width = 1200;
eCanvas.height = 1000;
var ctx = eCanvas.getContext("2d");

/* ****************************** */
/*           Objekten             */
/* ****************************** */


// Ladda in bilderna


/* ****************************** */
/*     Starta animationsloopen    */
/* ****************************** */
loopen();

/* ****************************** */
/*            Funktioner          */
/* ****************************** */

    
// Animationsloopen
function loopen() {
    // Sudda ut hela canvas
    ctx.clearRect(0, 0, 1200, 1000);

    // Rita ut figurerna
 

    requestAnimationFrame(loopen);
}
```

### Infoga en figur

* Vi infogar en figur på en slumpmässig position i x och y

```javascript
/* ****************************** */
/*           Objekten             */
/* ****************************** */
var pidgey = {
    x: Math.random() * 1000,
    y: -Math.random() * 500,
    bild: new Image()
};

// Ladda in bilderna
pidgey.bild.src = "./icon8/icons8-pidgey-50.png";

/* ****************************** */
/*     Starta animationsloopen    */
/* ****************************** */
loopen();

/* ****************************** */
/*            Funktioner          */
/* ****************************** */

// Rita ut pidgey
function ritaPidgey() {
    ctx.drawImage(pidgey.bild, pidgey.x, pidgey.y);
}
    
// Animationsloopen
function loopen() {
    // Sudda ut hela canvas
    ctx.clearRect(0, 0, 1200, 1000);

    // Rita ut figurerna
    ritaPidgey();
    ritaPsyduck();
    ritaDesura();
    ritaEevee();
    ritaMeowth();

    requestAnimationFrame(loopen);
}
```

### Figuren faller

* För varje loop i animationsloopen ökas y med 1, figuren ritas lite nedåt för varje loop&#x20;

```javascript
// Rita ut pidgey
function ritaPidgey() {
    pidgey.y++;
    ctx.drawImage(pidgey.bild, pidgey.x, pidgey.y);
}
```

### Figuren börjar om

* När figuren kommer ned till y = 1000px återställer vi på position y = 0

```javascript
// Rita ut pidgey
function ritaPidgey() {
    pidgey.y++;
    if (pidgey.y > 1000) {
        pidgey.y = 0;
        pidgey.x = Math.random() * 1000;
    }
    ctx.drawImage(pidgey.bild, pidgey.x, pidgey.y);
}
```
