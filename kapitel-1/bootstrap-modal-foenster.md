---
description: Bootstrap-biblioteket är ett populärt ramverk
---

# Modal-fönster med Bootstrap

## Bootstrap

![](<../.gitbook/assets/image (13).png>)

### Infoga biblioteket Bootstrap

* [https://getbootstrap.com/docs/5.0/getting-started/introduction/](https://getbootstrap.com/docs/5.0/getting-started/introduction/)

### Startkod

{% tabs %}
{% tab title="modal.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap modal</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Bootstrap modal-fönster</h1>

        <main>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Öppna modal med knapp
            </button>
            <p><a class="minLogin" href="#" data-bs-whatever="Test">Öppna modal med länk</a></p>
        </main>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossorigin="anonymous"></script>
    <script src="./modal.js"></script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Damion|Roboto+Slab&display=swap');

body {
    background: url("https://images.unsplash.com/photo-1515449634394-7f0f1d7d6543?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1351&q=80") fixed;
}
.kontainer {
    width: 700px;
    padding: 2em;
    margin: 1em auto;
    min-height: 89vh;
    background: #f5f5f5e0;
    border-radius: 5px;
    font-family: 'Roboto Slab', serif;
    border: 1px solid #a0a0a0;
    box-shadow: 1px 1px 4px #686868;
    text-align: center;
}
nav {
    padding: 1em 0;
}
main {
    margin: 2em 0;
    padding: 3em;
    font-size: 0.9em;
    color: #7b7b7b;
    background: #FFF;
}

nav .anamn {
    margin-left: auto;
    padding: 10px;
    font-weight: bold;
}

form {
    background: #ffffffa3;
    border: 1px solid #a0a0a0;
    padding: 2em;
    border-radius: 5px;
}
form label {
    color: #555555;
    font-weight: bold;
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
form input, form textarea {
    padding: 0.5em;
    margin-top: -0.4em;
    font-style: italic;
    border-radius: 0.3em;
    border: 2px solid #55a5d2;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
form textarea {
    height: 10em;
}
form button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
form img {
    width: 30px;
}
form .grid {
    display: grid;
    grid-template-columns: 1fr 1fr 2fr;
    gap: 1em;
}
form .result {
    text-align: right;
    color: #cc2a2a;
    font-style: italic;
    align-self: center;
}


h1, h2, h3, h4, h5, h6 {
    font-size: 1em;
    font-weight: bold;
}
h1, h2, h3, p {
    margin: 0.8em 0;
}
h1 {
    color: #ffffff;
    font-size: 3em;
    margin: 0;
    text-shadow: 1px 1px 1px #5c5c5c;
}

table {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid #b89e44;
    background: #ffffffa3;
    padding: 2em;
    border-radius: 5px;
}
th, td {
    padding: 0.5em;
    text-align: left;
}
th {
    background: #FFF;
    padding: 1em 0.5em;
}
tr:nth-child(even) {
    background: #f1f1f1;
}
/* tr:nth-child(odd) {
    background: #FFF;
} */
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}

.grid-6 {
    padding: 2em;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    gap: 2em;
}
```
{% endtab %}
{% endtabs %}

### Infoga ett modal-fönster

{% tabs %}
{% tab title="modal.js" %}
```javascript
const eLogin = document.querySelector(".minLogin");
const eModal = document.querySelector('#exampleModal');

var myModal = new bootstrap.Modal(eModal, {
    keyboard: false
})

eLogin.addEventListener("click", function () {
    console.log('eLogin.addEventListener("click")');
    myModal.show();
})

eModal.addEventListener("show.bs.modal", function (e) {
    console.log('eModal.addEventListener("show.bs.modal")');

    const modalTitle = eModal.querySelector('.modal-title');
    const modalBody = eModal.querySelector('.modal-body');

    modalTitle.textContent = 'Inloggning';
    modalBody.innerHTML = '<form>' +
    '<label>Användarnamn <input type="text" name="anamn" required></label>' +
    '<label>Lösenord <input type="password" name="lösen" required></label>' +
    '<div class="grid">' +
    '<div class="result"></div>' +
    '</div>' +
    '</form>';
})
```
{% endtab %}
{% endtabs %}
