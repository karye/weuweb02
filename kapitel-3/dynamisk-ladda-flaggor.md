---
description: Med fetch() kan man kommunicera med ett backend-skript
---

# Ladda flaggor dynamiskt

## Frontend

{% tabs %}
{% tab title="flaggor.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ladda flaggor</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Alla världens flaggor</h1>
        <div class="grid-6"></div>
        <button class="btn btn-primary">Hämta flaggor</button>
    </div>
    <script src="./skript.js"></script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Damion|Roboto+Slab&display=swap');

body {
    background: url("https://images.unsplash.com/photo-1514454529242-9e4677563e7b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80") fixed;
    background-attachment: fixed;
    background-size: 1900px;
}
.kontainer {
    width: 700px;
    padding: 2em;
    margin: 1em auto;
    min-height: 89vh;
    background: #ffffff6e;
    border-radius: 5px;
    font-family: 'Roboto Slab', serif;
    border: 1px solid #a0a0a0;
    box-shadow: 1px 1px 4px #686868;
    text-align: center;
}
nav {
    padding: 1em 0;
}
main {
    padding: 1em;
    font-size: 0.9em;
    color: #7b7b7b;
}

nav .anamn {
    margin-left: auto;
    padding: 10px;
    font-weight: bold;
}

form {
    background: #ffffffa3;
    border: 1px solid #a0a0a0;
    padding: 2em;
    border-radius: 5px;
}
form label {
    color: #555555;
    font-weight: bold;
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
form input, form textarea {
    padding: 0.5em;
    margin-top: -0.4em;
    font-style: italic;
    border-radius: 0.3em;
    border: 2px solid #55a5d2;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
form textarea {
    height: 10em;
}
form button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
form img {
    width: 30px;
}
form .grid {
    display: grid;
    grid-template-columns: 1fr 1fr 2fr;
    gap: 1em;
}
form .result {
    text-align: right;
    color: #cc2a2a;
    font-style: italic;
    align-self: center;
}


h1, h2, h3, h4, h5, h6 {
    font-size: 1em;
    font-weight: bold;
}
h1, h2, h3, p {
    margin: 0.8em 0;
}
h1 {
    color: #ffffff;
    font-size: 3em;
    margin: 0;
    text-shadow: 1px 1px 1px #5c5c5c;
}

table {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid #b89e44;
    background: #ffffffa3;
    padding: 2em;
    border-radius: 5px;
}
th, td {
    padding: 0.5em;
    text-align: left;
}
th {
    background: #FFF;
    padding: 1em 0.5em;
}
tr:nth-child(even) {
    background: #f1f1f1;
}
/* tr:nth-child(odd) {
    background: #FFF;
} */
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}

.inlagg {
    background: #ffffffa3;
    border: 1px solid #b89e44;
    padding: 2em;
    margin-bottom: 2em;
    border-radius: 5px;
}

.modal-body {
    background: burlywood;
}
.grid-6 {
    padding: 2em;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    gap: 2em;
}
```
{% endtab %}
{% endtabs %}

* Ladda ned [flaggorna](https://github.com/HatScripts/circle-flags)

```javascript
/* 
En fetch-loader inspirerat av https://www.drakenfilm.se/sok
Flaggor från https://github.com/HatScripts/circle-flags
*/

// Väljer ut element
const eKnapp = document.querySelector("button");
const eGrid = document.querySelector(".grid-6");

// När man klickar på knappen
eKnapp.addEventListener("click", function() {
    console.log("Hämtar...");
    fetch("./backend/skicka-flaggor.php")
    .then(response => response.text())
    .then(data => {
        console.log(data);
        eGrid.innerHTML += data;
    })
    .catch(error => {
        console.log("Något gick fel: ", error);
    })
})
```

## PHP-backend

```php
<?php
session_start();
if (!isset($_SESSION["rad"])) {

    // Läs in alla rader
    $_SESSION["allaFlaggor"] = scandir("../circle-flags/flags");
    $_SESSION["rad"] = 2;
}
for ($i = $_SESSION["rad"]; $i < $_SESSION["rad"] + 18; $i++) {
    if ($i > count($_SESSION["allaFlaggor"]) - 2) {
        echo "<p>Inga fler flaggor</p>";
        break;
    }
    $flagga = $_SESSION["allaFlaggor"][$i];
    if (substr($flagga, -3) == "svg") {
        echo "<img src=\"./circle-flags/flags/$flagga\" alt=\"Flagga\">";
    }
}
$_SESSION["rad"] = $_SESSION["rad"] + 18;

```
